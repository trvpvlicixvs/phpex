<style>
.product {
	border-radius: 25px; 
	padding: 20px; 
	margin:10px;
	width: 100px;  
	border: 2px solid red;
	float: left;
	text-align: center; 
} 
.product h3 {
	padding:0px;
	margin:0px;
	margin-bottom:10px;
}
</style>
<?php
require "classes/Guitar.php";
$guitars = Guitar::getGuitars("data.txt");
foreach($guitars as $guitar){ ?>
	<div class='product'>
		<h3><?=$guitar->title?></h3>
		<img src='images/<?=$guitar->picture?>' width=100 /><br><br>
		Price: <br><b><?=$guitar->price?> EUR</b><br> 
		<a href='https://www.youtube.com/watch?v=<?=$guitar->ytlink?>' target='_blank'>See review</a>
	</div>
<?php } ?> 