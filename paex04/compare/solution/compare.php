<?php
echo "<a href='index.php'>Back to products</a><hr>";
require "Product.php";
$f = fopen("products.txt","r");
$targetRow1 = $_POST['p1'];
$targetRow2 = $_POST['p2'];  
$product1 = null;
$product2 = null;  
$detailTitles = explode(",",fgets($f));
$row = 1;   
while(!feof($f)){
	$rowString = fgets($f); 
	if($row==$targetRow1){
		$product1 = Product::parse($rowString);
	} else 
	if($row==$targetRow2){
		$product2 = Product::parse($rowString);
	}
	if($product1!=null&&$product2!=null){
		break;
	}
	$row++;
} 
fclose($f);  
if($product1==null||$product2==null){
	echo "No products to compare";
	return;
}
echo "<table border=1>";
echo "<tr style='font-weight:bold;'><td>Option</td><td>{$product1->title}<br><img src='images/{$product1->picture}' width=100 /></td><td>{$product2->title}<br><img src='images/{$product2->picture}' width=100 /></td></tr>";
foreach($detailTitles as $k=>$v){
	echo "<tr><td>{$v}</td><td>{$product1->options[$k]}</td><td>{$product2->options[$k]}</td></tr>";
}
echo "</table>";


