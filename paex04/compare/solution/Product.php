<?php
class Product {
	public $options = [];
	public $title;
	public $price;
	public $picture;
	public static function parse($row){
		$parts = explode("|",$row);
		$res = new Product;
		$res->title 	= $parts[0];
		$res->price 	= $parts[1];
		$res->picture 	= $parts[2];
		$res->options 	= array_slice($parts,3);
		return $res;
	}
}